# README #

#### On local machine: ####
1. Clone repository
2. install docker-desktop (https://www.docker.com/products/docker-desktop/)
3. install Maven (https://maven.apache.org/download.cgi)
4. Run following commands:
    a. mvn clean install
    b. docker-compose --env-file /env/.env.dev build --pull
    c. docker-compose --env-file /env/.env.dev up -d

### What is this repository for? ###

* This is marketdata-enhancer repository. The idea of this repository is to enhance the market data published by the marketdata-publisher
* The data is read from the input topic used by marketdata publisher, data is enhanced (like calculation of high, low prices etc) and published on an output topic
* The data from output topic is published to a websocket (ws-marketdata) on topic (/marketdataWSTopic/ws-marketdata-ind) and ready for consumption by client
* #### v1.0 ####

### How do I get set up? ###

* The application can be run locally or using docker. Refer to setup on local machine above for running on docker
* To run locally, just run MarketdataEnhancerApplication class on your favourite IDE
* No additional configuration is required
* The application connects to Kafka broker (kafka:9092 and localhost:29092 if running on local) and expects the broker to be up and running. 

### Contribution guidelines ###

### Who do I talk to? ###

* vkarkhanis@gmail.com