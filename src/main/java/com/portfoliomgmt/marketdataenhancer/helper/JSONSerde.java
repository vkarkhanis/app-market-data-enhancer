package com.portfoliomgmt.marketdataenhancer.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JSONSerde<T> implements Serializer<T>, Deserializer<T> {

    private Class<T> clazz;
    private ObjectMapper objectMapper;
    public static final String JSON_POJO_CLASS_KEY = "jsonPojoClass";

    public JSONSerde() {

        this.objectMapper = new ObjectMapper();
        this.objectMapper.registerModule(new JavaTimeModule());
        this.objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        this.clazz = (Class<T>)configs.get(JSON_POJO_CLASS_KEY);
    }

    @Override
    public T deserialize(String topic, byte[] bytes) {
        if (bytes == null)
            return null;

        T data;
        try {
            data = this.objectMapper.readValue(bytes, clazz);
        } catch (Exception e) {
            throw new SerializationException("Error deserializing the bytes",e);
        }

        return data;
    }

    @Override
    public byte[] serialize(String topic, T data) {
        if (data == null)
            return null;

        try {
            return objectMapper.writeValueAsBytes(data);
        } catch (Exception e) {
            throw new SerializationException("Error serializing JSON message", e);
        }
    }


    @Override
    public void close() {

    }
}
