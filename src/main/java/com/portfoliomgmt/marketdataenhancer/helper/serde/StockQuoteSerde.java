package com.portfoliomgmt.marketdataenhancer.helper.serde;

import com.portfoliomgmt.marketdataenhancer.helper.JSONSerde;
import com.portfoliomgmt.marketdataenhancer.model.StockQuote;

import java.util.HashMap;
import java.util.Map;

public class StockQuoteSerde extends JSONSerde<StockQuote> {

    public StockQuoteSerde() {
        Map<String, Object> serdeProps = new HashMap<>();
        serdeProps.put(JSONSerde.JSON_POJO_CLASS_KEY, StockQuote.class);
        configure(serdeProps, false);
    }

}
