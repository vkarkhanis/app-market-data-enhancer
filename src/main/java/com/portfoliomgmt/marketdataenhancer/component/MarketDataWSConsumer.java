package com.portfoliomgmt.marketdataenhancer.component;

import com.portfoliomgmt.marketdataenhancer.model.StockQuote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class MarketDataWSConsumer {

    private static final Logger logger = LoggerFactory.getLogger(MarketDataWSConsumer.class);

    @KafkaListener(topics = "${marketdata.kafka.destination.out}",
            containerFactory = "wsListenerContainerFactory")
    public void pushMarketData(StockQuote stockQuote) {
        template.convertAndSend(createTopicName(), stockQuote);
    }

    private String createTopicName() {
        return "/" + destinationPrefix + "/" + destination;
    }

    @Value("${marketdata.ws.destination.prefix}")
    private String destinationPrefix;

    @Value("${marketdata.ws.destination}")
    private String destination;

    @Autowired
    private SimpMessagingTemplate template;
}
