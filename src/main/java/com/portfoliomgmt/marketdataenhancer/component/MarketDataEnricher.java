package com.portfoliomgmt.marketdataenhancer.component;

import com.portfoliomgmt.marketdataenhancer.model.StockQuote;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

@Component
public class MarketDataEnricher {

    public StockQuote enhance(StockQuote oldData, StockQuote newData) {
        return new StockQuote(newData.getSymbol(),
                oldData.getHigh().max(newData.getPrice()),
                oldData.getLow().min(newData.getPrice()),
                newData.getOpen(), newData.getPrice(),
                newData.getVolume(), newData.getLastTradingDay(),
                newData.getPreviousClose(), calculateChange(oldData.getPrice(), newData.getPrice()),
                calculateChangePct(oldData.getPrice(), newData.getPrice()) + "%");
    }

    private double calculateChangePct(BigDecimal basePrice, BigDecimal newPrice) {
        return new BigDecimal(calculateChange(basePrice, newPrice))
                .divide(basePrice, 2, RoundingMode.HALF_UP).movePointLeft(2).doubleValue();
    }

    private double calculateChange(BigDecimal basePrice, BigDecimal newPrice) {
        return newPrice.subtract(basePrice).round(new MathContext(3)).doubleValue();
    }

}
