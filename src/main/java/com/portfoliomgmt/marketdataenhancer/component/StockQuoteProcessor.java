package com.portfoliomgmt.marketdataenhancer.component;

import com.portfoliomgmt.marketdataenhancer.helper.serde.StockQuoteSerde;
import com.portfoliomgmt.marketdataenhancer.model.StockQuote;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StockQuoteProcessor {

    private static final Logger logger = LoggerFactory.getLogger(StockQuoteProcessor.class);

    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder) {
        KStream<String, StockQuote> messageStream = streamsBuilder
                .stream(topicName, Consumed.with(Serdes.String(),
                        Serdes.serdeFrom(stockQuoteSerde, stockQuoteSerde)));

        messageStream.groupByKey().reduce(
                (resultantValue, newValue) -> marketDataEnricher.enhance(resultantValue, newValue)
        ).toStream().to(topicNameOut, Produced.with(Serdes.String(),
                Serdes.serdeFrom(stockQuoteSerde, stockQuoteSerde)));
    }

    @Value(value = "${marketdata.kafka.destination.in}")
    private String topicName;

    @Value(value = "${marketdata.kafka.destination.out}")
    private String topicNameOut;

    @Autowired
    private StockQuoteSerde stockQuoteSerde;

    @Autowired
    private MarketDataEnricher marketDataEnricher;

}
