package com.portfoliomgmt.marketdataenhancer.configuration;

import com.portfoliomgmt.marketdataenhancer.helper.JSONSerde;
import com.portfoliomgmt.marketdataenhancer.helper.serde.StockQuoteSerde;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;

import java.util.HashMap;
import java.util.Map;

import static org.apache.kafka.streams.StreamsConfig.*;

@Configuration
@EnableKafkaStreams
public class KafkaStreamConfiguration {

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    KafkaStreamsConfiguration kStreamsConfig() {
        Map<String, Object> props = new HashMap<>();
        props.put(APPLICATION_ID_CONFIG, "market-data-enhancer");
        props.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, JSONSerde.class);
        props.put(COMMIT_INTERVAL_MS_CONFIG, "500");

        return new KafkaStreamsConfiguration(props);
    }

    @Autowired
    private StockQuoteSerde stockQuoteSerde;

    @Value(value = "${bootstrap.servers}")
    private String bootstrapServer;
}
