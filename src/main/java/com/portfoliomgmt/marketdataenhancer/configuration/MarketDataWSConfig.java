package com.portfoliomgmt.marketdataenhancer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class MarketDataWSConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/" + stompEndpoint).setAllowedOrigins(allowedOrigins);
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry broker) {
        broker.enableSimpleBroker("/" + destinationPrefix);
        broker.setApplicationDestinationPrefixes("/" + appDesinationPrefix);
    }

    @Value("${marketdata.ws.destination.prefix}")
    private String destinationPrefix;

    @Value("${marketdata.ws.destination.allowedOrigins}")
    private String allowedOrigins;

    @Value("${marketdata.ws.destination.appDestinationPrefix}")
    private String appDesinationPrefix;

    @Value("${marketdata.ws.stomp.endpoint}")
    private String stompEndpoint;
}
