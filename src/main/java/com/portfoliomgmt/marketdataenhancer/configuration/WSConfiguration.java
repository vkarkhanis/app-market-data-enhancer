package com.portfoliomgmt.marketdataenhancer.configuration;

import com.portfoliomgmt.marketdataenhancer.model.StockQuote;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class WSConfiguration {

    @Bean
    public ConsumerFactory<String, StockQuote> wsConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, wsGroupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "com.portfoliomgmt.marketdatasimulator.model.*");

        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(), new JsonDeserializer<>(StockQuote.class, false));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, StockQuote> wsListenerContainerFactory() {

        ConcurrentKafkaListenerContainerFactory<String, StockQuote> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(wsConsumerFactory());
        return factory;
    }

    @Value(value = "${bootstrap.servers}")
    private String bootstrapServer;

    @Value(value="${marketdata.kafka.group.id}")
    private String wsGroupId;
}
