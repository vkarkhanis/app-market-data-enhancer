package com.portfoliomgmt.marketdataenhancer.configuration;

import com.portfoliomgmt.marketdataenhancer.component.MarketDataEnricher;
import com.portfoliomgmt.marketdataenhancer.helper.serde.StockQuoteSerde;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MarketEnhancerConfig {

    @Bean
    public StockQuoteSerde stockQuoteSerde() {
        return new StockQuoteSerde();
    }

    @Bean
    public MarketDataEnricher marketDataEnhancer() {
        return new MarketDataEnricher();
    }
}
