package com.portfoliomgmt.marketdataenhancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketdataEnhancerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketdataEnhancerApplication.class, args);
	}

}
